# String to Number Converter

How to nicely format floating numbers to String without unnecessary decimal 0.

## Installation

You need to have maven installed on your machine to run this project.
Please built the project before running

```bash
mvn clean install
```

## Usage

``` java
BaseConverter.convert("25.560"); // returns "25.56"
BaseConverter.convert("05.20"); // returns "5.2"
```

## Contributing
Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.
Please make sure to update tests as appropriate.

## License
No License (Keep up the good work:))