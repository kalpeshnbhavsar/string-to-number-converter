package com.kb;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.Arrays;
import java.util.List;

public class MainClassTest {

    private List<String> listInput, listOutput, listOutputWithLimit;

    @Before
    public void setUp() {

        listInput = Arrays.asList("25.56789",
                "232.00000000",
                "1232.50000000",
                "0.18000000000",
                "1237875192.0",
                "4.5800000000",
                "1.23450000",
                "05.20",
                "0",
                "0.0",
                "1. 0",
                "2.4 0",
                null,
                "",
                " ",
                "invalid string here");


        listOutput = Arrays.asList("25.56789",
                "232",
                "1232.5",
                "0.18",
                "1237875192",
                "4.58",
                "1.2345",
                "5.2",
                "0",
                "0",
                null,
                null,
                null,
                null,
                null,
                null);


        listOutputWithLimit = Arrays.asList("25.5",
                "232",
                "1232.5",
                "0.1",
                "1237875192",
                "4.5",
                "1.2",
                "5.2",
                "0",
                "0",
                null,
                null,
                null,
                null,
                null,
                null);

    }

    @Test
    public void testConvert() {
        for (int i = 0; i < listInput.size(); i++) {
            Assert.assertEquals(listOutput.get(i), BaseConverter.convert(listInput.get(i)));
        }
    }

    @Test
    public void testConvertWithLimit() {
        for (int i = 0; i < listInput.size(); i++) {
            Assert.assertEquals(listOutputWithLimit.get(i), BaseConverter.convertWithLimit(listInput.get(i),"###.#"));
        }
    }
}
