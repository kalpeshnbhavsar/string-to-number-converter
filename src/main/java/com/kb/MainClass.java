package com.kb;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Arrays;
import java.util.List;

public class MainClass {

    public static void main(String[] args) {

        Logger log = LoggerFactory.getLogger(MainClass.class);

        List<String> list = Arrays.asList("25.56789",
                "232.00000000",
                "1232.00000000",
                "0.18000000000",
                "1237875192.0",
                "4.5800000000",
                "1.23450000",
                "05.20",
                "0",
                "0.0",
                "1. 0",
                "2.4 0",
                null,
                "",
                " ",
                "invalid string here");

        list.forEach(s -> log.info(BaseConverter.convert(s)));
        log.info("-------------------");
        list.forEach(s -> log.info(BaseConverter.convertWithLimit(s,"###.#")));
    }
}
