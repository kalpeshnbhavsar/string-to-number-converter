package com.kb;

import java.text.DecimalFormat;
import java.math.RoundingMode;

class BaseConverter {

    private BaseConverter() {
    }

    static String convert(String s) {
        if (s != null && !s.isEmpty()) {
            try {
                return (stringFormatter(s));
            } catch (Exception e) {
                return null;
            }
        } else return null;
    }

    private static String stringFormatter(String s) {
        double d = Double.parseDouble(s);
        if (d == (long) d)
            return String.format("%d", (long) d);
        else
            return String.format("%s", d);
    }

    static String convertWithLimit(String s, String pattern) {
        if (s != null && !s.isEmpty() && pattern!=null) {
            try {
                DecimalFormat decimalFormat = new DecimalFormat(pattern);
                decimalFormat.setRoundingMode(RoundingMode.DOWN);
                return (decimalFormat.format(Double.parseDouble(s)));
            } catch (Exception e) {
                return null;
            }
        } else return null;
    }
}
